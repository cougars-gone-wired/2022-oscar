package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

public class Intake {
    private static final double INTAKE_SPEED = 0.75;

    private int intakeAxisID;

    private WPI_TalonSRX intakeMotor;

    //sets up the intake class to be called later
    public Intake(int intakeAxisID) {
        this.intakeAxisID = intakeAxisID;

        intakeMotor = new WPI_TalonSRX(Constants.IntakeConstants.INTAKE_MOTOR_ID);
        initialize();
    }

    //starts the intake motors in a non moving state
    public void initialize() {
        setNotMoving();
    }

    public enum IntakeStates { NOT_MOVING, INTAKING, OUTTAKING }

    private IntakeStates currentIntakeState;

    //the primary intake control class
    public void controlIntake() {
        double intakeAxis = -Robot.manipulatorController.getRawAxis(intakeAxisID);

        //uses a switch function to determine wheter to start or stop intaking and outtaking
        switch (currentIntakeState) {
            
            //sets the robot to start intaking if the robot's arms are in the right position
            case NOT_MOVING:
                if (intakeAxis >= Constants.IntakeConstants.DEADZONE && Robot.arms.isArmShootingPosition() && !Robot.arms.isArmClimbingPosition()) setIntaking();
                
                else if (intakeAxis <= -Constants.IntakeConstants.DEADZONE && !Robot.arms.isIntakeUpPositon() && Robot.arms.isArmShootingPosition() && !Robot.arms.isArmClimbingPosition()) setOuttaking();
                
                break;

            //tells the robot to stop intaking if the robots arms are in the right position
            case INTAKING:
                if (Robot.arms.isIntakeUpPositon()) setStop();
                else setIntaking();

                if (intakeAxis < Constants.IntakeConstants.DEADZONE || !Robot.arms.isArmShootingPosition() || Robot.arms.isArmClimbingPosition()) setNotMoving();

                break;

            //tells the robot to start outtaking if the arms are in the right position
            case OUTTAKING:
                if (intakeAxis > -Constants.IntakeConstants.DEADZONE || Robot.arms.isIntakeUpPositon() || !Robot.arms.isArmShootingPosition() || Robot.arms.isArmClimbingPosition()) setNotMoving();
                
                break;
        }
    }

    //sets up some variables
    public boolean isNotMoving() {
        return currentIntakeState == IntakeStates.NOT_MOVING;
    }

    public boolean isIntaking() {
        return currentIntakeState == IntakeStates.INTAKING;
    }

    public boolean isOuttaking() {
        return currentIntakeState == IntakeStates.OUTTAKING;
    }

    public void setNotMoving() {
        intakeMotor.set(0);
        currentIntakeState = IntakeStates.NOT_MOVING;
    }

    public void setIntaking() {
        intakeMotor.set(INTAKE_SPEED);
        currentIntakeState = IntakeStates.INTAKING;
    }

    public void setOuttaking() {
        intakeMotor.set(-INTAKE_SPEED);
        currentIntakeState = IntakeStates.OUTTAKING;
    }

    public void setStop() {
        intakeMotor.set(0);
    }

    public void setMotorsBrake() {
        intakeMotor.setNeutralMode(NeutralMode.Brake);
    }

    public void setMotorsCoast() {
        intakeMotor.setNeutralMode(NeutralMode.Coast);
    }
} 