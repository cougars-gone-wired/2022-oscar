package frc.robot.autoPrograms;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
// import edu.wpi.first.wpilibj.Timer;
// import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class DriveForward {    

    double startingDistance;
    double currentDistance;

    public DriveForward() {
        
        
    }

    public void initalize() {
        Robot.encoders.resetEncoders();

        startingDistance = Robot.encoders.getAvgEncoderMetersAvg();
        currentDistance = Robot.encoders.getAvgEncoderMetersAvg();
    }

    public void execute() {
        
        SmartDashboard.putNumber("Starting Distance", startingDistance);
        currentDistance = Robot.encoders.getAvgEncoderMetersAvg();
        SmartDashboard.putNumber("Current Distance", currentDistance);

        if ((Math.abs(currentDistance) - Math.abs(startingDistance)) <= 0.75) {
            Robot.drive.driveStraight(0.23);

        } else {
            Robot.drive.driveStraight(0);
        }
    }
}
