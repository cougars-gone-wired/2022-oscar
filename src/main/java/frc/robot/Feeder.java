package frc.robot;

// import frc.robot.Robot;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

// import edu.wpi.first.wpilibj.DigitalInput;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

// the system that "feeds" balls from the intake to the shooter
// assists in intaking balls and feeds shooter when shooting
public class Feeder {
    private static final double FEED_SHOOTER_SPEED = 0.82; // speed for feeding balls to the shooter
    // private static final double AUTO_FEED_SHOOTER_SPEED = 0.7; // speed for feeding balls to the shooter in auto (slower since driver can't fix jams)
    private static final double FEED_INTAKE_SPEED = 0.6; // speed for intaking balls into the feeder

    private int feederShooterTriggerID;

    private WPI_TalonSRX feederMotor;

    // line breaks used to indicate when the feeder had 4 balls in it, so that the feeder could stop intaking while the 
    // intake could continue to get one more ball, commented out in code since testing was not completed
    // private DigitalInput feederUpperLineBreak;
    // private DigitalInput feederLowerLineBreak;

    private boolean feederTriggerActive;
    // private double feedShooterSpeed; // the speed at which the feeder feeds balls into the shooter when shooting

    public Feeder(int feederShooterTriggerID) {
        this.feederShooterTriggerID = feederShooterTriggerID;

        feederMotor = new WPI_TalonSRX(Constants.FeederConstants.FEEDER_MOTOR_ID);
        // feederUpperLineBreak = new DigitalInput(Constants.FeederConstants.FEEDER_UPPER_LINEBREAK_PORT);
        // feederLowerLineBreak = new DigitalInput(Constants.FeederConstants.FEEDER_LOWER_LINEBREAK_PORT);
        initialize();
    }

    public void initialize() {
        setNotMoving();
    }

    public enum FeederStates {
        NOT_MOVING, INTAKING, SHOOTING, OUTTAKING
    }

    private FeederStates currentFeederState;

    public void controlFeeder() {
        // SmartDashboard.putBoolean("Upper Line Break", feederUpperLineBreak.get());
        // SmartDashboard.putBoolean("Lower Line Break", feederLowerLineBreak.get());

        feederTriggerActive = (Robot.manipulatorController.getRawAxis(feederShooterTriggerID) >= Constants.FeederConstants.DEADZONE);

        switch (currentFeederState) {
            case NOT_MOVING:
                    if (Robot.shooter.isShooting()) {
                        setShooting();
                    } else if (Robot.intake.isIntaking()) {
                        setIntaking();
                    }
                break;

            case INTAKING:
                    if (!Robot.intake.isIntaking()) {
                        setNotMoving();
                    }
                break;

            case SHOOTING:
                    if (feederTriggerActive) {
                        feederMotor.set(FEED_SHOOTER_SPEED);
                    }
                    else {
                        feederMotor.set(0);
                    }

                    if (!Robot.shooter.isShooting()) {
                        setNotMoving();
                    }
                break;

            case OUTTAKING:
                break;
        }
    }

    public void setNotMoving() {
        feederMotor.set(0);
        currentFeederState = FeederStates.NOT_MOVING;
    }

    public void setIntaking() {
        feederMotor.set(FEED_INTAKE_SPEED);
        currentFeederState = FeederStates.INTAKING;
    }
    
    public void setShooting() {
        feederMotor.set(FEED_SHOOTER_SPEED);
        currentFeederState = FeederStates.SHOOTING;
    }

    public void setMotorsBrake() {
        feederMotor.setNeutralMode(NeutralMode.Brake);
    }

    public void setMotorsCoast() {
        feederMotor.setNeutralMode(NeutralMode.Coast);
    }
}
