// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
// import frc.robot.autoPrograms.DoNothing;
import frc.robot.autoPrograms.DriveForward;
import edu.wpi.first.wpilibj.Joystick;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
    
    public static Joystick mobilityController;
    public static Joystick manipulatorController;

    public static Drive drive;
    public static Arms arms;
    public static Feeder feeder;
    public static Intake intake;
    public static Climber climber;
    public static Shooter shooter;
    public static Limelight limelight;
    public static Chomper chomper;
    public static Encoders encoders;

    public static DriveForward autoProgram;

    /**
     * This function is run when the robot is first started up and should be used for any
     * initialization code.
     */
    @Override
    public void robotInit() {
        
        mobilityController = new Joystick(Constants.ControllerConstants.MOBILITY_CONTROLLER_ID);
        manipulatorController = new Joystick(Constants.ControllerConstants.MANIPULATOR_CONTROLLER_ID);

        drive = new Drive(Constants.ControllerConstants.DRIVE_SPEED_AXIS, Constants.ControllerConstants.DRIVE_TURN_AXIS, Constants.ControllerConstants.DRIVE_SIDE_TOGGLE);
        arms = new Arms(Constants.ControllerConstants.ARM_UP_BUMPER, Constants.ControllerConstants.ARM_DOWN_BUMPER, Constants.ControllerConstants.INTAKE_ARM_UP_BUMPER, Constants.ControllerConstants.INTAKE_ARM_DOWN_BUMPER);
        feeder = new Feeder(Constants.ControllerConstants.SHOOTER_TRIGGER);
        intake = new Intake(Constants.ControllerConstants.INTAKE_AXIS);
        climber = new Climber(Constants.ControllerConstants.CLIMBER_UP_TRIGGER, Constants.ControllerConstants.CLIMBER_DOWN_TRIGGER);
        shooter = new Shooter(Constants.ControllerConstants.SHOOTER_TOGGLE);
        limelight = new Limelight(Constants.ControllerConstants.LIMELIGHT_BUTTON);

        feeder = new Feeder(Constants.ControllerConstants.SHOOTER_TRIGGER);
        chomper = new Chomper(Constants.ControllerConstants.CHOMPER_OVERRIDE_AXIS);

        encoders = new Encoders(drive);

    }

    /**
     * This function is called every robot packet, no matter the mode. Use this for items like
     * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
     *
     * <p>This runs after the mode specific periodic functions, but before LiveWindow and
     * SmartDashboard integrated updating.
     */
    @Override
    public void robotPeriodic() {
        
    }

    /**
     * This autonomous (along with the chooser code above) shows how to select between different
     * autonomous modes using the dashboard. The sendable chooser code works with the Java
     * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
     * uncomment the getString line to get the auto name from the text box below the Gyro
     *
     * <p>You can add additional auto modes by adding additional comparisons to the switch structure
     * below with additional strings. If using the SendableChooser make sure to add them to the
     * chooser code above as well.
     */
    @Override
    public void autonomousInit() {
        
        setMotorsBrake();

        drive.initalize();
        arms.initialize();
        intake.initialize();
        climber.initalize();
        shooter.initialize();
        limelight.dashboardInitialize(); //this one might not belong here, but everything else it was grouped with was removed from robotInit
        limelight.initialize();
        limelight.turnlightsOn();
        feeder.initialize();
        chomper.initialize();

        autoProgram = new DriveForward();
        autoProgram.initalize();

    }

    /** This function is called periodically during autonomous. */
    @Override
    public void autonomousPeriodic() {
        autoProgram.execute();
    }

    /** This function is called once when teleop is enabled. */
    @Override
    public void teleopInit() {

        setMotorsBrake();

        drive.initalize();
        // arms.initialize();
        intake.initialize();
        climber.initalize();
        shooter.initialize();
        limelight.initialize();
        feeder.initialize();
        chomper.initialize();

    }

    /** This function is called periodically during operator control. */
    @Override
    public void teleopPeriodic() {
        
        shooter.setDashboardValues();
        drive.setDashboardValues();

        arms.controlShooterArm();
        // climber.climberControls();
        drive.robotDrive();
        shooter.controlShooter();
        shooter.putRealVelocity();
        feeder.controlFeeder();
        limelight.limelightDrive();
        chomper.controlChomper();

        arms.controlIntakeArm();
        intake.controlIntake();

        // shooter.putRealVelocity();
    }

    /** This function is called once when the robot is disabled. */
    @Override
    public void disabledInit() {

        setMotorsCoast();
        limelight.turnlightsOff();
        chomper.initialize();
    }

    /** This function is called periodically when disabled. */
    @Override
    public void disabledPeriodic() {

    }

    /** This function is called once when test mode is enabled. */
    @Override
    public void testInit() {

    }

    /** This function is called periodically during test mode. */
    @Override
    public void testPeriodic() {


    }


    public void setMotorsBrake() {
        climber.setMotorsBrake();
        drive.setMotorsBrake();
        intake.setMotorsBrake();
        feeder.setMotorsBrake();
    }

    public void setMotorsCoast() {
        climber.setMotorsCoast();
        drive.setMotorsCoast();
        intake.setMotorsCoast();
        feeder.setMotorsCoast();
    }
}
